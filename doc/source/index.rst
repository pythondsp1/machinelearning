.. Machine learning guide documentation master file, created by
   sphinx-quickstart on Fri Mar  3 10:57:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Machine learning guide
======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   machinelearning/index 

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
